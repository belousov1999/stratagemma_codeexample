﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OLS.Features.Cards.Data.SO.Cards
{
    [CreateAssetMenu(fileName = "CardsDataSO", menuName = "SO/CardsDataSO", order = 51)]
    public class CardsDataSO : ScriptableObject
    {
        [SerializeField] public List<CardData> CardDatas;
    }

    [Serializable]
    public class CardData
    {
        public string CardNameKey;
        public string CardDescriptionKey;
        public List<CardCost> CardCurrency;
    }

    [Serializable]
    public class CardCost
    {
        public string Key;
        public int Amount;
    }
}