﻿using System.Collections.Generic;
using OLS.Features.Cards.Data.SO.Cards;
using OLS.Features.Cards.Data.Views;

namespace Features.Cards.Data.Models
{
    public class CardModel
    {
        public readonly string CardNameKey;
        public readonly string CardDescriptionKey;
        public readonly List<CardCurrency> CardCost;
        public CardView View;

        public CardModel(CardView view, CardData data)
        {
            CardCost = new List<CardCurrency>();
            
            View = view;
            CardNameKey = data.CardNameKey;
            CardDescriptionKey = data.CardDescriptionKey;
            
            foreach (var cardCost in data.CardCurrency)
            {
                CardCurrency currency = new CardCurrency(cardCost.Key, cardCost.Amount);
                CardCost.Add(currency);
            }
            
            View.SetContent(CardNameKey, CardDescriptionKey,CardCost[0].Amount.ToString());
        }
    }
}