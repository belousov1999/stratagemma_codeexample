﻿namespace Features.Cards.Data.Models
{
    public class CardCurrency
    {
        public string Key;
        public int Amount;

        public CardCurrency(string key, int amount)
        {
            Key = key;
            Amount = amount;
        }
    }
}