﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace OLS.Features.Cards.Data.Views
{
    public class CardView : MonoBehaviour
    {
        [SerializeField] private Text cardNameText;
        [SerializeField] private Text cardCostText;
        [SerializeField] private Text cardDescriptionText;
        private int EcsEntityId = -1;
        
        public RectTransform rectTransform;
        public Tweener TweenerMove;
        public Tweener TweenerRotate;
        public Tweener TweenerScale;

        public void InitCardView(int entityId, float moveDuration, float rotateDuration, float scaleDuration)
        {
            EcsEntityId = entityId;
            
            TweenerMove = DOTween.To(() => rectTransform.anchoredPosition,
                    (pos) => rectTransform.anchoredPosition = pos,
                    rectTransform.anchoredPosition, moveDuration)
                .SetEase(Ease.OutQuint).SetAutoKill(false);
            
            TweenerRotate = DOTween.To(() => rectTransform.rotation,
                    (pos) => rectTransform.rotation = pos,
                    rectTransform.rotation.eulerAngles, rotateDuration)
                .SetEase(Ease.OutQuint).SetAutoKill(false);
            
            TweenerScale = DOTween.To(() => rectTransform.localScale,
                    (pos) => rectTransform.localScale = pos,
                    rectTransform.localScale, scaleDuration)
                .SetEase(Ease.OutQuint).SetAutoKill(false);
        }
        
        public int GetEntityId()
        {
            return EcsEntityId;
        }

        public void SetContent(string Name, string Description, string Cost)
        {
            cardNameText.text = Name;
            cardDescriptionText.text = Description;
            cardCostText.text = Cost;
        }
    }
}