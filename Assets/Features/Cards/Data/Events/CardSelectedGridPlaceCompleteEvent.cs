﻿using Features.BaseEcs.Data;

namespace OLS.Features.Cards.Data.Events
{
    public struct CardSelectedGridPlaceCompleteEvent: IEventComponent
    {
        public int EntityId { get; set; }
        public string SenderSystem { get; set; }
        public int SenderEntityId { get; set; }
    }
}