﻿using Features.BaseEcs.Data;

namespace OLS.Features.Cards.Data.Events
{
    public struct CardsTimerExpiredEvent: IEventComponent
    {
        public string SenderSystem { get; set; }
        public int SenderEntityId { get; set; }
        public int EntityId { get; set; }
    }
}