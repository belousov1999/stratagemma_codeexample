﻿using Features.BaseEcs.Data;

namespace OLS.Features.Cards.Data.Events
{
    public struct CardCreateHandDeckEvent: IEventComponent
    {
        public int EntityId { get; set; }
        public string SenderSystem { get; set; }
        public int SenderEntityId { get; set; }
    }
}