﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Features.Cards.Middle
{
    public struct CardHandDeckPosition
    {
        public Vector2 Position;
        public Vector2 lastPosition;
        
        public Quaternion lastRotation;
        public Quaternion Rotation;

        public Vector3 Scale;
        public int CanvasSiblingIndex;
    }
}