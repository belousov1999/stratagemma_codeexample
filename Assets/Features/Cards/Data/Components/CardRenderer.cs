﻿using UnityEngine;

namespace OLS.Features.Cards.Data.Components
{
    public struct CardRenderer
    {
        public RectTransform Transform;
    }
}