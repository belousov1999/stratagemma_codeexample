﻿using UnityEngine;

namespace OLS.Features.Cards.Data.Components
{
    public struct CardIncrease
    {
        public Vector2 CardPosition;
        public Vector3 CardRotation;
        public Vector3 CardScale;
    }
}