﻿using DG.Tweening;
using Features.BaseEcs.Data;

namespace OLS.Features.Cards.Data.Components
{
    public struct Card : IEcsComponentWithId
    {
        public int EntityId { get; set; }
    }
}