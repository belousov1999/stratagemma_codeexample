﻿using Features.BaseEcs.Data;
using UnityEngine;

namespace OLS.Features.Cards.Data.Components
{
    public struct CardsMainUI : IEcsComponentWithId
    {
        public Transform Transform;

        public int EntityId { get; set; }
    }
}