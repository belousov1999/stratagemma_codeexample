using System.Collections;
using System.Collections.Generic;
using Leopotam.EcsLite;
using UnityEngine;

public static class CardsWorlds
{
    public const string Cards = nameof(Cards);

    public static readonly EcsWorld.Config CardsConfig = new(); 
}
