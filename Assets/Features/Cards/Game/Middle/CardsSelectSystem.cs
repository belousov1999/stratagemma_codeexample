﻿using Features.BaseEcs.Data;
using Features.BaseEcs.Game.Base;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Cards.Data.Events;
using UnityEngine;

namespace Features.Cards.Middle
{
    public class CardsSelectSystem : IEcsInitSystem, IEcsRunSystem
    {
        [EcsInject] private EventsManagerSystem eventsManagerSystem;

        private EcsPool<CardSelected> cardsSelectedPool;
        private EcsPool<CardReadyToRemove> cardsReadyToRemovePool;

        private EcsPool<CardSelectedEvent> cardsSelectedEventPool;
        private EcsPool<CardSelectedGridPlaceStartEvent> cardsSelectedGridPlaceStartEventPool;
        private EcsPool<CardSelectedGridPlaceCompleteEvent> cardsSelectedGripPlaceCompleteEventPool;
        private EcsPool<CardsClearSelectedEvent> CardsClearSelectedEventPool;
        private EcsPool<CardCreateHandDeckEvent> cardsCreateHandDeckEventPool;

        private EcsFilter cardsFilter;

        public void Init(IEcsSystems systems)
        {
            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);

            var eventsWorld = systems.GetWorld(BaseEcsWorlds.Events);

            cardsSelectedPool = cardsWorld.GetPool<CardSelected>();
            cardsReadyToRemovePool = cardsWorld.GetPool<CardReadyToRemove>();
            
            cardsFilter = cardsWorld.Filter<Card>().Inc<CardIncrease>().End();

            cardsSelectedEventPool = eventsWorld.GetPool<CardSelectedEvent>();
            cardsSelectedGridPlaceStartEventPool = eventsWorld.GetPool<CardSelectedGridPlaceStartEvent>();
            cardsSelectedGripPlaceCompleteEventPool = eventsWorld.GetPool<CardSelectedGridPlaceCompleteEvent>();
            CardsClearSelectedEventPool = eventsWorld.GetPool<CardsClearSelectedEvent>();
            cardsCreateHandDeckEventPool = eventsWorld.GetPool<CardCreateHandDeckEvent>();
        }

        public void Run(IEcsSystems systems)
        {
            if (cardsFilter.GetEntitiesCount() <= 0)
            {
                return;
            }

            var cards = cardsFilter.GetRawEntities();

            var cardEntityId = cards[0];

            if (Input.GetMouseButtonDown(0))
            {
                if (cardsSelectedPool.Has(cardEntityId) == false)
                {
                    ref var cardSelected = ref cardsSelectedPool.Add(cardEntityId);
                    cardSelected.isHide = false;

                    ref var cardSelectedEvent = ref eventsManagerSystem.SendEvent(cardsSelectedEventPool, cardEntityId,
                        nameof(CardsSelectSystem));
                    cardSelectedEvent.isSelect = true;

                    ref var cardSelectedGridPlaceStart = ref eventsManagerSystem.SendEvent(
                        cardsSelectedGridPlaceStartEventPool,
                        -1, nameof(CardsSelectSystem));
                    cardSelectedGridPlaceStart.BuildingName = "DoorArcPrefab";
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (cardsSelectedPool.Has(cardEntityId))
                {
                    ref var cardSelectedEvent = ref eventsManagerSystem.SendEvent(cardsSelectedEventPool, cardEntityId,
                        nameof(CardsSelectSystem));
                    cardSelectedEvent.isSelect = false;

                    var cardSelected = cardsSelectedPool.Get(cardEntityId);
                    cardsSelectedPool.Del(cardEntityId);
                    
                    if (cardSelected.isGoodPosition)
                    {
                        
                        cardsReadyToRemovePool.Add(cardEntityId);
                        
                        eventsManagerSystem.SendEvent(CardsClearSelectedEventPool, cardEntityId,
                            nameof(CardsSelectSystem));
                        
                        eventsManagerSystem.SendEvent(cardsCreateHandDeckEventPool, -1, nameof(CardsSelectSystem));
                    }
                    
                    eventsManagerSystem.SendEvent(cardsSelectedGripPlaceCompleteEventPool, -1, nameof(CardsSelectSystem));
                }
            }
        }
    }
}