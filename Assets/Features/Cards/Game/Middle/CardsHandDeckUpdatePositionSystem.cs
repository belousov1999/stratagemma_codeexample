﻿using DG.Tweening;
using Features.BaseEcs.Game;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;

namespace Features.Cards.Middle
{
    public class CardsHandDeckUpdatePositionSystem : IEcsInitSystem, IEcsRunSystem
    {
        [EcsInject] private CardsManagerSystem cardsManagerSystem;

        private EcsPool<Card> cardsPool;
        private EcsPool<CardHandDeckPosition> cardsHandDeckUpdatePositionPool;

        private EcsFilter cardsSortMoveFilter;

        public void Init(IEcsSystems systems)
        {
            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);

            cardsPool = cardsWorld.GetPool<Card>();
            cardsSortMoveFilter = cardsWorld.Filter<Card>().Inc<CardHandDeckPosition>().Exc<CardIncrease>().Exc<CardSelected>().Exc<CardReadyToRemove>().End();
            cardsHandDeckUpdatePositionPool = cardsWorld.GetPool<CardHandDeckPosition>();
        }

        public void Run(IEcsSystems systems)
        {
            cardsPool.Iterate(cardsSortMoveFilter, OnCardsSortMove);
        }

        private void OnCardsSortMove(int entityId, Card component)
        {
            ref var cardUpdatePositionComponent = ref cardsHandDeckUpdatePositionPool.Get(entityId);

            var cardModel = cardsManagerSystem.GetCardModelByEntityId(entityId);

            if (cardModel == null)
            {
                return;
            }

            cardModel.View.TweenerMove.ChangeEndValue(cardUpdatePositionComponent.Position, true).Restart();
            cardModel.View.TweenerRotate.ChangeEndValue(cardUpdatePositionComponent.Rotation.eulerAngles, true).Restart();
            cardModel.View.transform.SetSiblingIndex(cardUpdatePositionComponent.CanvasSiblingIndex);
        }
    }
}