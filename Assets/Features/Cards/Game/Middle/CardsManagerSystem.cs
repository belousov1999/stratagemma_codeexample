﻿using System.Collections.Generic;
using Features.BaseEcs.Data;
using Features.BaseEcs.Game;
using Features.BaseEcs.Game.Base;
using Features.Cards.Data.Models;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Cards.Data.Events;
using OLS.Features.Cards.Data.SO.Cards;
using OLS.Features.Cards.Data.Views;
using UnityEngine;

namespace Features.Cards.Middle
{
    public class CardsManagerSystem : IEcsInitSystem
    {
        [EcsInject] private ContentManager contentManager;
        [EcsInject] private EventsManagerSystem eventsManagerSystem;
        
        private const float CardTweenerMoveDuration = 1f;
        private const float CardTweenerRotateDuration = 1f;
        private const float CardTweenerScaleDuration = 0.05f;
        
        private List<CardModel> CardModels;
        private CardView cardViewPrefab;
        private CardsDataSO cardsDataSO;
        
        private EcsPool<Card> cardsPool;
        private EcsPool<CardCreateHandDeckEvent> cardsCreatedNotHandSortedEventPool;
        private EcsPool<CardRenderer> cardsRendererPool;

        public void Init(IEcsSystems systems)
        {
            CardModels = new List<CardModel>();

            var eventsWorld = systems.GetWorld(BaseEcsWorlds.Events);
            cardsCreatedNotHandSortedEventPool = eventsWorld.GetPool<CardCreateHandDeckEvent>();

            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);
            cardsPool = cardsWorld.GetPool<Card>();
            cardsRendererPool = cardsWorld.GetPool<CardRenderer>();

            var cardPrefab = contentManager.LoadAssetAsync<GameObject>("CardView").WaitForCompletion();
            cardViewPrefab = cardPrefab.GetComponent<CardView>();

            cardsDataSO = contentManager.LoadAssetAsync<CardsDataSO>("CardsDataSO").WaitForCompletion();
        }

        public void InitCardContent(string cardNameKey, Transform parent)
        {
            var cardData = GetCardDataInScriptableObject(cardNameKey);

            if (cardData == null)
            {
                return;
            }
            
            var view = GameObject.Instantiate(cardViewPrefab, parent);

            ref var card = ref cardsPool.NewWithId();
            ref var cardRenderer = ref cardsRendererPool.Add(card.EntityId);
            
            cardRenderer.Transform = view.rectTransform;
            
            view.InitCardView(card.EntityId, CardTweenerMoveDuration, CardTweenerRotateDuration, CardTweenerScaleDuration);
            view.gameObject.name = "CardView " + card.EntityId;

            CardModel model = new CardModel(view, cardData);

            CardModels.Add(model);

            eventsManagerSystem.SendEvent(cardsCreatedNotHandSortedEventPool, card.EntityId,
                nameof(CardsManagerSystem));
        }

        public void RemoveCardModel(int entityId)
        {
            var model = GetCardModelByEntityId(entityId);
            CardModels.Remove(model);
        }
        
        public CardModel GetCardModelByEntityId(int entityId)
        {
            foreach (var model in CardModels)
            {
                if (model.View.GetEntityId() == entityId)
                {
                    return model;
                }
            }

            return null;
        }

        private CardData GetCardDataInScriptableObject(string cardNameKey)
        {
            foreach (var cardData in cardsDataSO.CardDatas)
            {
                if (cardData.CardNameKey == cardNameKey)
                {
                    return cardData;
                }
            }

            return null;
        }
    }
}