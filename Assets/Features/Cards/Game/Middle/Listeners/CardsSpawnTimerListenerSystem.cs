﻿using Features.BaseEcs.Game.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS;
using OLS.Events;
using OLS.Features.Cards.Data.Components;

namespace Features.Cards.Middle.Listeners
{
    public class CardsSpawnTimerListenerSystem: EventListenerSystem<TimerExpiredEvent>
    {
        [EcsInject] private CardsManagerSystem cardsManagerSystem;
        [EcsInject] private CardsInitTimerSystem cardsInitTimerSystem;
        private EcsPool<CardsMainUI> cardsMainUIPool;
        private EcsPool<CardsMaxCountMainUI> cardsMaxCountMainUIPool;
        private EcsPool<CardsTimer> cardsTimerPool;
        private EcsFilter cardsMainUIFilter;
        private EcsFilter cardsFilter;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);
            var cardsWorlds = systems.GetWorld(CardsWorlds.Cards);
            var timersWorld = systems.GetWorld(TimersWorlds.Timers);

            cardsMainUIPool = cardsWorlds.GetPool<CardsMainUI>();
            cardsMaxCountMainUIPool = cardsWorlds.GetPool<CardsMaxCountMainUI>();
            cardsTimerPool = timersWorld.GetPool<CardsTimer>();
            cardsMainUIFilter = cardsWorlds.Filter<CardsMainUI>().End(1);
            cardsFilter = cardsWorlds.Filter<Card>().End();
        }

        protected override void OnEvent(int eventEntityId, TimerExpiredEvent component)
        {
            if (!cardsTimerPool.Has(component.SenderEntityId))
            {
                return;
            }
            var cardsCount = cardsFilter.GetEntitiesCount();
                
            var cardsMainUIEntities = cardsMainUIFilter.GetRawEntities();
            var cardsMainUICanvas = cardsMainUIPool.Get(cardsMainUIEntities[0]);
                
            var cardsMaxCount = cardsMaxCountMainUIPool.Get(cardsMainUICanvas.EntityId).CardsMaxCount;
                
            if (cardsMaxCount <= cardsCount)
            {
                return;
            }
                
            var cardsMainUITransform = cardsMainUICanvas.Transform;

            cardsManagerSystem.InitCardContent("House",cardsMainUITransform);
        }
    }
}