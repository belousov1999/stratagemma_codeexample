﻿using DG.Tweening;
using Features.BaseEcs.Game.Middle;
using Features.Cards.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Cards.Data.Events;
using UnityEngine;

namespace OLS.Features.Cards.Game.Middle.Listeners
{
    public class CardsSelectedListener: EventListenerSystem<CardSelectedEvent>
    {
        [EcsInject] private CardsManagerSystem cardsManagerSystem;
        
        private EcsPool<CardIncrease> cardsIncreasePool;
        private EcsPool<CardRenderer> cardsRendererPool;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);

            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);

            cardsIncreasePool = cardsWorld.GetPool<CardIncrease>();
            cardsRendererPool = cardsWorld.GetPool<CardRenderer>();
        }

        protected override void OnEvent(int eventEntityId, CardSelectedEvent component)
        {
            var cardEntityId = component.SenderEntityId;
            
            var model = cardsManagerSystem.GetCardModelByEntityId(cardEntityId);

            if (model == null)
            {
                return;
            }
            
            ref var cardIncrease = ref cardsIncreasePool.Get(cardEntityId);
            ref var cardRenderer = ref cardsRendererPool.Get(cardEntityId);
            
            var positionY = cardRenderer.Transform.anchoredPosition.y;
            float positionTo = 0f;
            
            if (component.isSelect == false)
            {
                positionTo = positionY - 30;

                cardIncrease.CardPosition = new Vector2(cardIncrease.CardPosition.x, positionTo);
            }
            else
            {
                positionTo = positionY + 30;

                cardIncrease.CardPosition = new Vector2(cardIncrease.CardPosition.x, positionTo);
            }
            
            model.View.TweenerMove
                .ChangeEndValue(cardIncrease.CardPosition, true)
                .Restart();
        }
    }
}