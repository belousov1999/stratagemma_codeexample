﻿using Features.BaseEcs.Data;
using Features.BaseEcs.Game.Base;
using Features.BaseEcs.Game.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Events;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Cards.Data.Events;
using OLS.Features.Timers.Data.Components;


namespace OLS.Features.Cards.Game.Middle.Listeners
{
    public class CardTimerCreateRendererSystem : EventListenerSystem<CardsTimerCreatedEvent>
    {
        [EcsInject] private EventsManagerSystem eventsManagerSystem;
        private EcsPool<TimerTextRenderer> timerTextRendererPool;
        private EcsPool<CardsMainUI> cardsMainUIPool;
        private EcsPool<CardsTimerRenderer> cardsTimerRendererPool;
        private EcsPool<TimerTextRenderCreateEvent> timersTextRendererCreateEventPool;
        
        private EcsFilter cardsMainUIFilter;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);
            
            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);
            var timersWorld = systems.GetWorld(TimersWorlds.Timers);
            var eventsWorld = systems.GetWorld(BaseEcsWorlds.Events);

            timerTextRendererPool = timersWorld.GetPool<TimerTextRenderer>();
            cardsMainUIPool = cardsWorld.GetPool<CardsMainUI>();
            cardsTimerRendererPool = timersWorld.GetPool<CardsTimerRenderer>();
            timersTextRendererCreateEventPool = eventsWorld.GetPool<TimerTextRenderCreateEvent>();
            
            cardsMainUIFilter = cardsWorld.Filter<CardsMainUI>().End(1);
        }

        protected override void OnEvent(int eventEntityId, CardsTimerCreatedEvent component)
        {
            var cardsMainUIEntities = cardsMainUIFilter.GetRawEntities();;
            ref var cardsMainUICanvas = ref cardsMainUIPool.Get(cardsMainUIEntities[0]);
            
            ref var cardsTimerRenderer = ref cardsTimerRendererPool.Get(component.SenderEntityId);

            ref var timerCreateEvent = ref eventsManagerSystem.SendEvent(timersTextRendererCreateEventPool, cardsTimerRenderer.TimerTextEntityId,
                nameof(CardTimerCreateRendererSystem));

            timerCreateEvent.TransformParent = cardsMainUICanvas.Transform;
            timerCreateEvent.ResourceTextName = "Timer";
        }
    }
}