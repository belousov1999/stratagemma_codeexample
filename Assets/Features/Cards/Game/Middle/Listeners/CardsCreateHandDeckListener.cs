﻿using Features.BaseEcs.Game.Middle;
using Features.Cards.Middle;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Cards.Data.Events;
using UnityEngine;

namespace OLS.Features.Cards.Game.Middle.Listeners
{
    public class CardsCreateHandDeckListener : EventListenerSystem<CardCreateHandDeckEvent>
    {
        private const float CardsPositionOffsetX = 50f;
        private const float CardsPositionOffsetY = 25f;
        private const float CardsAngleOffset = 15f;
        private const float CardsAngleAndPositionMultiplier = 1.5f;

        private readonly Vector2 centerDeckPosition = Vector2.zero;

        private EcsPool<CardHandDeckPosition> cardsHandDeckPositionPool;
        private EcsPool<CardRenderer> cardsRendererPool;
        
        private EcsFilter cardsPoolFilter;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);

            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);
            
            cardsRendererPool = cardsWorld.GetPool<CardRenderer>();
            cardsHandDeckPositionPool = cardsWorld.GetPool<CardHandDeckPosition>();
            
            cardsPoolFilter = cardsWorld.Filter<Card>().End();
        }

        protected override void OnEvent(int eventEntityId, CardCreateHandDeckEvent component)
        {
            var cardsCount = cardsPoolFilter.GetEntitiesCount();
            
            var cardsAngleOffset = CardsAngleOffset;
            var cardsPositionOffset = CardsPositionOffsetX;
            var unEvenCardsCount = true;

            if (cardsCount % 2 == 0)
            {
                unEvenCardsCount = false;
                cardsAngleOffset = CardsAngleOffset / CardsAngleAndPositionMultiplier;
                cardsPositionOffset = CardsPositionOffsetX / CardsAngleAndPositionMultiplier;
            }

            var halfCardsCount = cardsCount / 2;
            
            var leftCardsCount = 0;
            var rightCardsCount = halfCardsCount;

            var cardCounter = 0;
            
            foreach (var cardEntityIndex in cardsPoolFilter)
            {
                ref var updatePositionComponent = ref GetOrAddUpdatingPosition(cardEntityIndex);
                updatePositionComponent.Scale = new Vector3(1, 1, 1);
                updatePositionComponent.CanvasSiblingIndex = cardCounter;
                
                float rotY = 0;
                float posX = 0;
                float posY = 0;
                
                if (cardCounter == halfCardsCount && unEvenCardsCount)
                {
                    updatePositionComponent.Rotation = Quaternion.Euler(centerDeckPosition);
                    updatePositionComponent.Position = new Vector2(centerDeckPosition.x, halfCardsCount * -CardsPositionOffsetY / 3);
                    cardCounter++;
                    continue;
                }

                if (cardCounter > halfCardsCount - 1)
                {
                    rotY = -cardsAngleOffset * (leftCardsCount + 1);
                    posX = cardsPositionOffset * (leftCardsCount + 1);
                    posY = -CardsPositionOffsetY * (leftCardsCount + 1);
                    leftCardsCount++;
                }
                else
                {
                    rotY = cardsAngleOffset * rightCardsCount;
                    posX = -cardsPositionOffset * rightCardsCount;
                    posY = -CardsPositionOffsetY * rightCardsCount;
                    rightCardsCount--;
                }

                updatePositionComponent.Rotation = Quaternion.Euler(new Vector3(0f, 0f, rotY));
                updatePositionComponent.Position = new Vector2(posX, posY);
                cardCounter++;
            }
        }

        private ref CardHandDeckPosition GetOrAddUpdatingPosition(int cardEntityIndex)
        {
            var cardTransform = cardsRendererPool.Get(cardEntityIndex).Transform;
            
            if (cardsHandDeckPositionPool.Has(cardEntityIndex))
            {
                ref var updatePositionComponent = ref cardsHandDeckPositionPool.Get(cardEntityIndex);
                updatePositionComponent.lastRotation = cardTransform.rotation;
                updatePositionComponent.lastPosition = cardTransform.anchoredPosition;
                return ref updatePositionComponent;
            }
            else
            {
                ref var updatePositionComponent = ref cardsHandDeckPositionPool.Add(cardEntityIndex);
                updatePositionComponent.lastRotation = cardTransform.rotation;
                updatePositionComponent.lastPosition = cardTransform.anchoredPosition;
                return ref updatePositionComponent;
            }
        }
    }
}
