﻿using Features.BaseEcs.Game.Middle;
using Features.Cards.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Events;

namespace OLS.Features.Cards.Game.Middle.Listeners
{
    public class CardsClearSelectedSystem : EventListenerSystem<CardsClearSelectedEvent>
    {
        [EcsInject] private CardsManagerSystem cardsManagerSystem;

        private EcsWorld cardsWorld;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);
            cardsWorld = systems.GetWorld(CardsWorlds.Cards);
        }

        protected override void OnEvent(int eventEntityId, CardsClearSelectedEvent component)
        {
            var entityId = component.SenderEntityId;

            cardsManagerSystem.RemoveCardModel(entityId);
            cardsWorld.DelEntity(entityId);
        }
    }
}