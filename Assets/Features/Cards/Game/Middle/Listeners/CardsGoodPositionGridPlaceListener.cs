﻿using Features.BaseEcs.Game.Middle;
using Features.Cards.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Cards.Data.Events;

namespace OLS.Features.Cards.Game.Middle.Listeners
{
    public class CardsGoodPositionGridPlaceListener: EventListenerSystem<CardGoodPositionGridPlaceEvent>
    {
        [EcsInject] private CardsManagerSystem cardsManagerSystem;
        
        private EcsPool<CardSelected> cardsSelectedPool;
        
        private EcsFilter cardSelectedFilter;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);
            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);

            cardsSelectedPool = cardsWorld.GetPool<CardSelected>();
            cardSelectedFilter = cardsWorld.Filter<Card>().Inc<CardSelected>().End();
        }

        protected override void OnEvent(int eventEntityId, CardGoodPositionGridPlaceEvent component)
        {
            var isHide = component.isGoodPosition;
            
            foreach (var cardEntityId in cardSelectedFilter)
            {
                var model = cardsManagerSystem.GetCardModelByEntityId(cardEntityId);
                model.View.gameObject.SetActive(!isHide);
                ref var cardSelected = ref cardsSelectedPool.Get(cardEntityId);
                cardSelected.isHide = isHide;
                cardSelected.isGoodPosition = component.isGoodPosition;
            }
        }
    }
}