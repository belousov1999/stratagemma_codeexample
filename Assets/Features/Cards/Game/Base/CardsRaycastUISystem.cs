﻿using System.Collections.Generic;
using Features.BaseEcs.Data;
using Features.BaseEcs.Game.Base;
using Features.Cards.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Cards.Data.Events;
using OLS.Features.Cards.Data.Views;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Features.Cards.Game.Base
{
    public class CardsRaycastUISystem : IEcsInitSystem, IEcsRunSystem
    {
        [EcsInject] private CardsManagerSystem cardsManagerSystem;
        [EcsInject] private EventsManagerSystem eventsManagerSystem;

        private readonly Vector3 selectedCardScale = new Vector3(1.5f, 1.5f, 1.5f);
        private readonly Vector3 selectedCardRotation = Vector3.zero;
        private readonly float selectedCardPositionY = 40f;

        private EcsPool<CardIncrease> cardsIncreasePool;

        private EcsPool<CardIncreaseEvent> cardsIncreaseEventPool;
        private EcsPool<CardDecreaseEvent> cardsDecreaseEventPool;
        private EcsPool<CardHandDeckPosition> cardsHandDeckPositionPool;

        private EcsFilter cardsIncreaseFilter;

        private PointerEventData pointerEventData;

        public void Init(IEcsSystems systems)
        {
            pointerEventData = new PointerEventData(EventSystem.current);

            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);

            var eventsWorld = systems.GetWorld(BaseEcsWorlds.Events);

            cardsIncreasePool = cardsWorld.GetPool<CardIncrease>();
            cardsHandDeckPositionPool = cardsWorld.GetPool<CardHandDeckPosition>();
            
            cardsIncreaseFilter = cardsWorld.Filter<Card>().Inc<CardIncrease>().Exc<CardSelected>().End();

            cardsIncreaseEventPool = eventsWorld.GetPool<CardIncreaseEvent>();
            cardsDecreaseEventPool = eventsWorld.GetPool<CardDecreaseEvent>();
        }

        public void Run(IEcsSystems systems)
        {
            var cardsEntityId = RaycastCardView();

            DecreaseCards(cardsEntityId);

            if (cardsEntityId == null || cardsEntityId.Count == 0)
            {
                return;
            }

            if (cardsIncreasePool.Has(cardsEntityId[0]) == false)
            {
                IncreaseCard(cardsEntityId[0]);
            }
        }

        private void DecreaseCards(List<int> cardsEntityId)
        {
            int currentIncreasedCardId = -1;
            
            if (cardsEntityId != null && cardsEntityId.Count != 0)
            {
                currentIncreasedCardId = cardsEntityId[0];
            }
            
            foreach (var cardId in cardsIncreaseFilter)
            {
                if (cardId != currentIncreasedCardId)
                {
                    eventsManagerSystem.SendEvent(cardsDecreaseEventPool, cardId, nameof(CardsRaycastUISystem));
                }
            }
        }

        private List<int> RaycastCardView()
        {
            List<int> cardsEntity = new List<int>();

            pointerEventData.position = Input.mousePosition;

            List<RaycastResult> results = new List<RaycastResult>();

            EventSystem.current.RaycastAll(pointerEventData, results);

            if (results.Count == 0)
            {
                return null;
            }

            foreach (var result in results)
            {
                if (result.gameObject.TryGetComponent(out CardView view))
                {
                    cardsEntity.Add(view.GetEntityId());
                }
            }

            return cardsEntity;
        }

        private void IncreaseCard(int cardEntityId)
        {
            ref var cardIncrease = ref cardsIncreasePool.Add(cardEntityId);

            var cardHandDeckPosition = cardsHandDeckPositionPool.Get(cardEntityId);
            var anchoredPosition = cardHandDeckPosition.Position;

            var selectedCardPositionX = anchoredPosition.x;

            if (anchoredPosition.x > 0)
            {
                selectedCardPositionX += 50f;
            }
            else if (anchoredPosition.x < 0)
            {
                selectedCardPositionX -= 50f;
            }


            cardIncrease.CardPosition = new Vector2(selectedCardPositionX, selectedCardPositionY);
            cardIncrease.CardRotation = selectedCardRotation;
            cardIncrease.CardScale = selectedCardScale;

            eventsManagerSystem.SendEvent(cardsIncreaseEventPool, cardEntityId, nameof(CardsRaycastUISystem));
        }
    }
}