﻿using Features.BaseEcs.Data;
using Features.BaseEcs.Game;
using Features.BaseEcs.Game.Base;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Cards.Data.Events;

namespace Features.Cards
{
    public class CardsInitTimerSystem: IEcsInitSystem
    {
        [EcsInject] private EventsManagerSystem eventsManagerSystem;
        [EcsInject] private TimersSystem timersSystem;
        
        private EcsPool<CardsTimerCreatedEvent> cardsTimerCreatedPool;
        
        public void Init(IEcsSystems systems)
        {
            var eventsWorld = systems.GetWorld(BaseEcsWorlds.Events);
            var timersWorld = systems.GetWorld(TimersWorlds.Timers);

            cardsTimerCreatedPool = eventsWorld.GetPool<CardsTimerCreatedEvent>();
            var timerTextPool = timersWorld.GetPool<TimerText>();
            var cardsTimerPool = timersWorld.GetPool<CardsTimer>();
            var cardsTimerRendererPool = timersWorld.GetPool<CardsTimerRenderer>();

            var timerEntityId = timersSystem.CreateTimer(5, true);
            ref var timerText = ref timerTextPool.NewWithId();
            timerText.TimerEntityId = timerEntityId;

            cardsTimerPool.Add(timerEntityId);
            ref var cardsTimerRenderer = ref cardsTimerRendererPool.Add(timerEntityId);
            cardsTimerRenderer.TimerTextEntityId = timerText.EntityId;
            
            eventsManagerSystem.SendEvent(cardsTimerCreatedPool, timerEntityId, nameof(CardsInitTimerSystem));
        }
    }
}