﻿using Features.BaseEcs.Game;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;
using UnityEngine;

namespace Features.Cards
{
    public class InitCardsUISystem : IEcsInitSystem
    {
        [EcsInject] private ContentManager contentManager;
        
        public void Init(IEcsSystems systems)
        {
            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);
            var cardsMainUIPool = cardsWorld.GetPool<CardsMainUI>();
            var cardsMaxMainUIPool = cardsWorld.GetPool<CardsMaxCountMainUI>();
            
            var canvasGameObjectPrefab = contentManager.LoadAssetAsync<GameObject>("Canvas").WaitForCompletion();
            var canvasGO = GameObject.Instantiate(canvasGameObjectPrefab);
            
            ref var cardCanvas = ref cardsMainUIPool.NewWithId();
            ref var cardsMaxCountEntity = ref cardsMaxMainUIPool.Add(cardCanvas.EntityId);
            cardsMaxCountEntity.CardsMaxCount = 2;
            cardCanvas.Transform = canvasGO.transform;
        }
    }
}