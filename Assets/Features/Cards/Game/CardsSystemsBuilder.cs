﻿using System.Collections.Generic;
using Features.Cards.Game.Base;
using Features.Cards.Game.Middle;
using Features.Cards.Middle;
using Features.Cards.Middle.Listeners;
using Leopotam.EcsLite;
using OLS.Core;
using OLS.Features.Cards.Game.Middle;
using OLS.Features.Cards.Game.Middle.Listeners;
using OLS.Features.Timers.Game.Middle;

namespace Features.Cards
{
    public class CardsSystemsBuilder : BaseFeatureBuilder
    {
        public override Dictionary<EcsWorld, string> GetFeatureWorlds()
        {
            return new Dictionary<EcsWorld, string>()
            {
                [new EcsWorld(CardsWorlds.CardsConfig)] = CardsWorlds.Cards
            };
        }
        
        public override IEcsSystem[] GetDataProviders()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetBaseSystems()
        {
            return new IEcsSystem[]
            {
                new InitCardsUISystem(),
                new CardsInitTimerSystem(),
                new CardsRaycastUISystem(),
                
                new CardsSelectSystem()
            };
        }

        public override IEcsSystem[] GetMiddleSystems()
        {
            return new IEcsSystem[]
            {
                new CardsManagerSystem(),
                
                new CardsSpawnTimerListenerSystem(),
                new CardTimerCreateRendererSystem(),
                new TimersTextRenderUpdateSystem(),
                
                new CardsClearSelectedSystem(),
                new CardsCreateHandDeckListener(),
                new CardsHandDeckUpdatePositionSystem(),
                
                new CardsSelectedListener(),
                
                new CardsGoodPositionGridPlaceListener(),
            };
        }

        public override IEcsSystem[] GetPostSystems()
        {
            return new IEcsSystem[]
            {
                new CardsIncreaseSystemListener(),
                new CardsDecreaseSystemListener(),
            };
        }
    }
}