﻿using DG.Tweening;
using Features.BaseEcs.Game.Middle;
using Features.Cards.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Cards.Data.Events;

namespace Features.Cards.Game.Middle
{
    public class CardsIncreaseSystemListener : EventListenerSystem<CardIncreaseEvent>
    {
        [EcsInject] private CardsManagerSystem cardsManagerSystem;
        
        private EcsPool<CardIncrease> cardsIncreasePool;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);
            
            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);
            
            cardsIncreasePool = cardsWorld.GetPool<CardIncrease>();
        }

        protected override void OnEvent(int eventEntityId, CardIncreaseEvent component)
        {
            var cardModel = cardsManagerSystem.GetCardModelByEntityId(component.SenderEntityId);
            
            if (cardModel == null)
            {
                return;
            }

            ref var cardIncrease = ref cardsIncreasePool.Get(component.SenderEntityId);
            
            cardModel.View.TweenerMove
                .ChangeEndValue(cardIncrease.CardPosition, true)
                .Restart();
            cardModel.View.TweenerRotate.ChangeEndValue(cardIncrease.CardRotation, true).Restart();
            cardModel.View.TweenerScale.ChangeEndValue(cardIncrease.CardScale, true).Restart();
            
            cardModel.View.transform.SetAsLastSibling();
        }
    }
}