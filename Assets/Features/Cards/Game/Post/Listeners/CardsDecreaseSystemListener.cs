﻿using DG.Tweening;
using Features.BaseEcs.Game.Middle;
using Features.Cards.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Cards.Data.Events;

namespace OLS.Features.Cards.Game.Middle
{
    public class CardsDecreaseSystemListener : EventListenerSystem<CardDecreaseEvent>
    {
        [EcsInject] private CardsManagerSystem cardsManagerSystem;

        private EcsPool<CardIncrease> cardsIncreasePool;
        private EcsPool<CardHandDeckPosition> cardsHandDeckPositionPool;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);
            var cardsWorld = systems.GetWorld(CardsWorlds.Cards);

            cardsIncreasePool = cardsWorld.GetPool<CardIncrease>();
            cardsHandDeckPositionPool = cardsWorld.GetPool<CardHandDeckPosition>();
        }

        protected override void OnEvent(int eventEntityId, CardDecreaseEvent component)
        {
            var cardEntityId = component.SenderEntityId;

            if (cardsIncreasePool.Has(cardEntityId) == false)
            {
                return;
            }

            cardsIncreasePool.Del(cardEntityId);
            
            var cardHandDeckPosition = cardsHandDeckPositionPool.Get(cardEntityId);
            var cardModel = cardsManagerSystem.GetCardModelByEntityId(cardEntityId);
            
            cardModel.View.TweenerMove
                .ChangeEndValue(cardHandDeckPosition.Position, true)
                .Restart();
            cardModel.View.TweenerRotate.ChangeEndValue(cardHandDeckPosition.Rotation.eulerAngles, true).Restart();
            cardModel.View.TweenerScale.ChangeEndValue(cardHandDeckPosition.Scale, true).Restart();
        }
    }
}