using System;
using Features.BaseEcs.Data;
using Features.BaseEcs.Game;
using Features.BaseEcs.Game.Base;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Events;

namespace OLS
{
    public class TimersSystem : IEcsInitSystem
    {
        [EcsInject] private EventsManagerSystem eventsManagerSystem;
        private EcsPool<Timer> timersPool;
        private EcsPool<TimerCreatedEvent> timerCreatedEventPool;

        public void Init(IEcsSystems systems)
        {
            var timersWorld = systems.GetWorld(TimersWorlds.Timers);
            var eventsWorld = systems.GetWorld(BaseEcsWorlds.Events);
            
            timersPool = timersWorld.GetPool<Timer>();
            timerCreatedEventPool = eventsWorld.GetPool<TimerCreatedEvent>();
        }

        public int CreateTimer(float durationSeconds, bool isRestarting)
        {
            ref var timer = ref timersPool.NewWithId();
            timer.Duration = durationSeconds;
            timer.CurrentTimeValue = durationSeconds;
            timer.Restart = isRestarting;
            timer.StartTime = DateTime.Now;
            timer.EndTime = timer.StartTime + TimeSpan.FromSeconds(durationSeconds);
            
            eventsManagerSystem.SendEvent(timerCreatedEventPool, timer.EntityId, nameof(TimersSystem));
            
            return timer.EntityId;
        }

        public void RestartTimer(int timerEntityId)
        {
            ref var timer = ref timersPool.Get(timerEntityId);
            timer.CurrentTimeValue = timer.Duration;
            timer.StartTime = DateTime.Now;
            timer.EndTime = timer.StartTime + TimeSpan.FromSeconds(timer.Duration);
        }
    }
}