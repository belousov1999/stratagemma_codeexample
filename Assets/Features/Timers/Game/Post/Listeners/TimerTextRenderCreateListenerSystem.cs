﻿using Features.BaseEcs.Game.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Events;
using OLS.Features.Timers.Data.Components;
using OLS.Features.Timers.Data.Components.Views;
using UnityEngine;
using UnityEngine.UI;

namespace OLS.Features.Timers.Game.Post.Listeners
{
    public class TimerTextRenderCreateListenerSystem: EventListenerSystem<TimerTextRenderCreateEvent>
    {
        [EcsInject] private ContentManager contentManager;
        
        private EcsPool<TimerTextRenderer> timerTextRendererPool;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);
            var timersWorld = systems.GetWorld(TimersWorlds.Timers);
            timerTextRendererPool = timersWorld.GetPool<TimerTextRenderer>();
        }

        protected override void OnEvent(int eventEntityId, TimerTextRenderCreateEvent component)
        {
            var prefab = contentManager.LoadAssetAsync<GameObject>(component.ResourceTextName)
                .WaitForCompletion();
            
            var timerGO = GameObject.Instantiate(prefab, component.TransformParent);
            var timerView = timerGO.GetComponent<TimerView>();
            
            ref var timerRenderer = ref timerTextRendererPool.Add(component.SenderEntityId);
            
            // TODO: Gabbasov Denis Replace with view
            timerRenderer.rectTransform = timerView.RectTransform;
            timerRenderer.TextField = timerView.Text;
        }
    }
}