﻿using Features.BaseEcs.Game;
using Leopotam.EcsLite;

namespace OLS.Post
{
    public class TimersClearSystem: IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld timersWorld;
        private EcsPool<Timer> timersPool;
        private EcsFilter timersExpiredFilter;

        public void Init(IEcsSystems systems)
        {
            timersWorld = systems.GetWorld(TimersWorlds.Timers);
            timersPool = timersWorld.GetPool<Timer>();
            
            timersExpiredFilter = timersWorld.Filter<Timer>().Inc<TimerExpired>().End();
        }

        public void Run(IEcsSystems systems)
        {
            timersPool.Iterate(timersExpiredFilter, OnTimerExpiredUpdate);
        }

        private void OnTimerExpiredUpdate(int entityId, Timer component)
        {
            timersWorld.DelEntity(entityId);
        }
    }
}