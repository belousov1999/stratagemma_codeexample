﻿using System;
using Features.BaseEcs.Game;
using Leopotam.EcsLite;
using UnityEngine;

namespace OLS.Features.Timers.Game.Middle
{
    public class TimersUpdateSystem: IEcsInitSystem, IEcsRunSystem
    {
        private EcsPool<Timer> timersPool;
        private EcsFilter timersPoolFilter;

        public void Init(IEcsSystems systems)
        {
            var timersWorld = systems.GetWorld(TimersWorlds.Timers);

            timersPool = timersWorld.GetPool<Timer>();
            timersPoolFilter = timersWorld.Filter<Timer>().End();
        }

        public void Run(IEcsSystems systems)
        {
            timersPool.Iterate(timersPoolFilter, OnTimerUpdate);
        }

        private void OnTimerUpdate(int entityId, ref Timer component)
        {
            var timeNow = DateTime.Now;
            component.CurrentTimeValue = component.EndTime.Subtract(timeNow).Seconds;
        }
    }
}