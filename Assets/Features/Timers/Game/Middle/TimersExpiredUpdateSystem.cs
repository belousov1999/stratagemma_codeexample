using System;
using Features.BaseEcs.Data;
using Features.BaseEcs.Game;
using Features.BaseEcs.Game.Base;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Events;

namespace OLS
{
    public class TimersExpiredUpdateSystem : IEcsInitSystem, IEcsRunSystem
    {
        [EcsInject] private EventsManagerSystem eventsManagerSystem;
        
        private EcsPool<TimerExpiredEvent> timersExpiredEventPool;
        private EcsPool<Timer> timersPool;
        private EcsPool<TimerExpired> timersExpiredPool;
        private EcsFilter timersNotExpiredFilter;

        public void Init(IEcsSystems systems)
        {
            var timersWorld = systems.GetWorld(TimersWorlds.Timers);
            var eventsWorld = systems.GetWorld(BaseEcsWorlds.Events);

            timersExpiredEventPool = eventsWorld.GetPool<TimerExpiredEvent>();
            timersPool = timersWorld.GetPool<Timer>();
            timersExpiredPool = timersWorld.GetPool<TimerExpired>();
            
            timersNotExpiredFilter = timersWorld.Filter<Timer>().Exc<TimerExpired>().End();
        }

        public void Run(IEcsSystems systems)
        {
            timersPool.Iterate(timersNotExpiredFilter, OnTimerUpdate);
        }

        private void OnTimerUpdate(int entityId, Timer component)
        {
            if (component.EndTime <= DateTime.Now)
            {
                timersExpiredPool.Add(entityId);
                
                eventsManagerSystem.SendEvent(timersExpiredEventPool, component.EntityId,
                    nameof(TimersExpiredUpdateSystem));
            }
        }
    }
}
