﻿using Features.BaseEcs.Game;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;

namespace OLS.Middle
{
    public class TimersRestartSystem: IEcsInitSystem, IEcsRunSystem
    {
        [EcsInject] private TimersSystem timersSystem;
        
        private EcsPool<Timer> timersPool;
        private EcsPool<TimerExpired> timersExpiredPool;
        private EcsFilter timersExpiredFilter;

        public void Init(IEcsSystems systems)
        {
            var timersWorld = systems.GetWorld(TimersWorlds.Timers);

            timersPool = timersWorld.GetPool<Timer>();
            timersExpiredPool = timersWorld.GetPool<TimerExpired>();
            timersExpiredFilter = timersWorld.Filter<Timer>().Inc<TimerExpired>().End();
        }

        public void Run(IEcsSystems systems)
        {
            timersPool.Iterate(timersExpiredFilter, OnTimerExpired);
        }

        private void OnTimerExpired(int entityId, Timer component)
        {
            if (component.Restart == false)
            {
                return;
            }
            
            timersSystem.RestartTimer(entityId);
            
            timersExpiredPool.Del(entityId);
        }
    }
}