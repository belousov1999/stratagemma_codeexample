﻿using Features.BaseEcs.Game;
using Leopotam.EcsLite;
using OLS.Features.Cards.Data.Components;
using OLS.Features.Timers.Data.Components;

namespace OLS.Features.Timers.Game.Middle
{
    public class TimersTextRenderUpdateSystem: IEcsInitSystem, IEcsRunSystem
    {
        private EcsPool<Timer> timersPool;
        private EcsPool<TimerTextRenderer> timerTextRendererPool;
        private EcsPool<TimerText> timerTextPool;

        private EcsFilter timersNotExpiredFilter;
        private EcsFilter timerTextFilter;

        public void Init(IEcsSystems systems)
        {
            var timersWorld = systems.GetWorld(TimersWorlds.Timers);

            timersPool = timersWorld.GetPool<Timer>();
            timerTextPool = timersWorld.GetPool<TimerText>();
            timerTextFilter = timersWorld.Filter<TimerText>().Inc<TimerTextRenderer>().End();
            timerTextRendererPool = timersWorld.GetPool<TimerTextRenderer>();
        }

        public void Run(IEcsSystems systems)
        {
            timerTextPool.Iterate(timerTextFilter, OnUpdateTimerText);
        }

        private void OnUpdateTimerText(int entityId, ref TimerText component)
        {
            ref var timer = ref timersPool.Get(component.TimerEntityId);
            ref var timerTextRenderer = ref timerTextRendererPool.Get(component.EntityId);
            timerTextRenderer.TextField.text = $"{timer.CurrentTimeValue}";
        }
    }
}