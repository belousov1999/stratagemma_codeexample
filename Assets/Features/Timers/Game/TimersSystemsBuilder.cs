using System.Collections.Generic;
using Leopotam.EcsLite;
using OLS.Core;
using OLS.Features.Timers.Game.Middle;
using OLS.Features.Timers.Game.Post.Listeners;
using OLS.Middle;
using OLS.Post;

namespace OLS
{
    public class TimersSystemsBuilder : BaseFeatureBuilder
    {
        public override Dictionary<EcsWorld, string> GetFeatureWorlds()
        {
            return new Dictionary<EcsWorld, string>()
            {
                [new EcsWorld(TimersWorlds.TimersConfig)] = TimersWorlds.Timers
            };
        }
        
        public override IEcsSystem[] GetDataProviders()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetBaseSystems()
        {
            return new IEcsSystem[]
            {
                new TimersSystem()
            };
        }

        public override IEcsSystem[] GetMiddleSystems()
        {
            return new IEcsSystem[]
            {
                new TimersUpdateSystem(),
                new TimersTextRenderUpdateSystem(),
                new TimersExpiredUpdateSystem(),
                new TimersRestartSystem()
            };
        }

        public override IEcsSystem[] GetPostSystems()
        {
            return new IEcsSystem[]
            {
                new TimerTextRenderCreateListenerSystem(),
                new TimersClearSystem()
            };
        }
    }
}