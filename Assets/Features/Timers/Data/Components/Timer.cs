using System;
using System.Collections;
using System.Collections.Generic;
using Features.BaseEcs.Data;
using UnityEngine;
using UnityEngine.Serialization;

namespace OLS
{
    public struct Timer : IEcsComponentWithId
    {
        public DateTime StartTime;
        public DateTime EndTime;
        
        public float CurrentTimeValue;
        public float Duration;
        public bool Restart;

        public int EntityId { get; set; }
    }
}