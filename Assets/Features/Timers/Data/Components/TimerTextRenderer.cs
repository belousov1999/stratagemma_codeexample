﻿using UnityEngine;
using UnityEngine.UI;

namespace OLS.Features.Timers.Data.Components
{
    public struct TimerTextRenderer
    {
        public RectTransform rectTransform;
        public Text TextField;
    }
}