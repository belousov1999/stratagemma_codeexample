﻿using Features.BaseEcs.Data;
using UnityEngine;

namespace OLS.Events
{
    public struct TimerTextRenderCreateEvent: IEventComponent
    {
        public string ResourceTextName;
        public Transform TransformParent;
        
        public int EntityId { get; set; }
        public string SenderSystem { get; set; }
        public int SenderEntityId { get; set; }
    }
}