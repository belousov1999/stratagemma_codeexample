﻿using Features.BaseEcs.Data;

namespace OLS.Events
{
    public struct TimerCreatedEvent: IEventComponent
    {
        public int EntityId { get; set; }
        public string SenderSystem { get; set; }
        public int SenderEntityId { get; set; }
    }
}