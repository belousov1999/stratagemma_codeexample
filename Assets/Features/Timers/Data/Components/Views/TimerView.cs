﻿using UnityEngine;
using UnityEngine.UI;

namespace OLS.Features.Timers.Data.Components.Views
{
    public class TimerView : MonoBehaviour
    {
        public RectTransform RectTransform;
        public Text Text;
    }
}