﻿using Features.BaseEcs.Data;

namespace OLS.Features.Cards.Data.Components
{
    public struct TimerText: IEcsComponentWithId
    {
        public int TimerEntityId;
        public int EntityId { get; set; }
    }
}