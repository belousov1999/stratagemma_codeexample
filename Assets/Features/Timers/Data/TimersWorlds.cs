using System.Collections;
using System.Collections.Generic;
using Leopotam.EcsLite;
using UnityEngine;

namespace OLS
{
    public static class TimersWorlds
    {
        public const string Timers = nameof(Timers);

        public static readonly EcsWorld.Config TimersConfig = new(); 
    }
}
