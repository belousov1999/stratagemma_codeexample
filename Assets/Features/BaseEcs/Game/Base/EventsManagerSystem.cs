﻿using Features.BaseEcs.Data;
using Leopotam.EcsLite;

namespace Features.BaseEcs.Game.Base
{
    public class EventsManagerSystem : IEcsInitSystem
    {
        public void Init(IEcsSystems systems)
        {
            
        }

        public ref T SendEvent<T>(EcsPool<T> pool, int entityId, string system) where T: struct, IEventComponent
        {
            ref var component = ref pool.NewWithId();
            component.SenderSystem = system; //DEBUG INFO
            component.SenderEntityId = entityId;
            
            return ref component;
        }
    }
}