﻿using Features.BaseEcs.Data;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;

namespace Features.BaseEcs.Game.Base
{
    public class DataProvidersLoader : IEcsInitSystem
    {
        [EcsInject] private ContentManager contentManager;
        
        public void Init(IEcsSystems systems)
        {
            var dataProviders = systems.GetSystems<IEcsDataProvider>();

            foreach (var dataProvider in dataProviders)
            {
                dataProvider.Init(contentManager);
            }
        }
    }
}