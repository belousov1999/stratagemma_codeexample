﻿using System.Collections.Generic;
using Features.BaseEcs.Data;
using Features.BaseEcs.Game.Base;
using Features.BaseEcs.Game.Middle;
using Features.BaseEcs.Game.Post;
using Leopotam.EcsLite;
using OLS.Core;

namespace Features.BaseEcs.Game
{
    public class BaseEcsSystemsBuilder : BaseFeatureBuilder
    {
        public override Dictionary<EcsWorld, string> GetFeatureWorlds()
        {
            return new Dictionary<EcsWorld, string>()
            {
                [new EcsWorld(BaseEcsWorlds.EventsConfig)] = BaseEcsWorlds.Events,
            };
        }
        
        public override IEcsSystem[] GetDataProviders()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetBaseSystems()
        {
            return new IEcsSystem[]
            {
                new DataProvidersLoader(),
                new EventsManagerSystem()
            };
        }

        public override IEcsSystem[] GetMiddleSystems()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetPostSystems()
        {
            return new IEcsSystem[]
            {
                new EventsClearSystem(),
            };
        }
    }
}