﻿using Features.BaseEcs.Data;
using Leopotam.EcsLite;

namespace Features.BaseEcs.Game.Middle
{
    public abstract class EventListenerSystem<T> : IEcsInitSystem, IEcsRunSystem 
        where T: struct, IEventComponent
    {
        protected EcsWorld EventsWorld;
        
        private EcsPool<T> eventPool;
        private EcsFilter eventFilter;

        public virtual void Init(IEcsSystems systems)
        {
            EventsWorld = systems.GetWorld(BaseEcsWorlds.Events);

            eventPool = EventsWorld.GetPool<T>();
            eventFilter = EventsWorld.Filter<T>().End();
        }

        public virtual void Run(IEcsSystems systems)
        {
            eventPool.Iterate(eventFilter, OnEvent);
        }

        protected abstract void OnEvent(int eventEntityId, T component);
    }
}