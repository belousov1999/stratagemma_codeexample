using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Features.BaseEcs.Data;
using Leopotam.EcsLite;

namespace Features.BaseEcs.Game
{
    public static class EcsPoolExtensions
    {
        public delegate void ActionRefStruct<T>(int entityId, ref T component) where T : struct;
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ref T GetOrAdd<T>(this EcsPool<T> pool, int entityId)
            where T : struct
        {
            if (pool.Has(entityId))
            {
                return ref pool.Get(entityId);
            }

            return ref pool.Add(entityId);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Del<T>(this EcsPool<T> pool, EcsFilter filter)
            where T : struct
        {
            foreach (var entityId in filter)
            {
                pool.Del(entityId);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DelAll<T>(this EcsPool<T> pool)
            where T : struct
        {
            var poolWorld = pool.GetWorld();
            var filter = poolWorld.Filter<T>().End(pool.GetRawDenseItemsCount());
            foreach (var entityId in filter)
            {
                pool.Del(entityId);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Del<T>(this EcsPool<T> pool, IEnumerable<int> filter)
            where T : struct
        {
            foreach (var entityId in filter)
            {
                pool.Del(entityId);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ref T New<T>(this EcsPool<T> pool)
            where T : struct
        {
            int entityId = pool.GetWorld().NewEntity();
            return ref pool.Add(entityId);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ref T NewWithId<T>(this EcsPool<T> pool)
            where T : struct, IEcsComponentWithId
        {
            int entityId = pool.GetWorld().NewEntity();
            ref var res = ref pool.Add(entityId);
            res.EntityId = entityId;
            return ref res;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Iterate(this IEcsPool pool, EcsFilter poolFilter, Action<int, object> processItem)
        {
            foreach (var entityId in poolFilter)
            {
                var component = pool.GetRaw(entityId);
                processItem(entityId, component);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Iterate<T>(this EcsPool<T> pool, Action<int, T> processItem)
            where T : struct
        {
            var poolWorld = pool.GetWorld();
            var filter = poolWorld.Filter<T>().End(pool.GetRawDenseItemsCount());
            Iterate(pool, filter, processItem);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Iterate<T>(this EcsPool<T> pool, ActionRefStruct<T> processItem)
            where T : struct
        {
            var poolWorld = pool.GetWorld();
            var filter = poolWorld.Filter<T>().End(pool.GetRawDenseItemsCount());
            Iterate(pool, filter, processItem);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Iterate<T>(this EcsPool<T> pool, EcsFilter poolFilter, Action<int, T> processItem)
            where T : struct
        {
            foreach (var entityId in poolFilter)
            {
                ref var component = ref pool.Get(entityId);
                processItem(entityId, component);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Iterate<T>(this EcsPool<T> pool, EcsFilter poolFilter, ActionRefStruct<T> processItem)
            where T : struct
        {
            foreach (var entityId in poolFilter)
            {
                ref var component = ref pool.Get(entityId);
                processItem(entityId, ref component);
            }
        }
    }
}