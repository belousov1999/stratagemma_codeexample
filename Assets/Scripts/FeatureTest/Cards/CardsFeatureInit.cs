using Cysharp.Threading.Tasks;
using Features.BaseEcs.Game;
using Features.Cards;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS;
using OLS.Core;
using UnityEngine;

public class CardsFeatureInit : MonoBehaviour
{
    private EcsSystems Systems;
    private ContentManager contentManager;

    public async UniTaskVoid Start()
    {
        await InitContentManager();
        InitSystems();
    }

    private void InitSystems()
    {
        Systems = new EcsSystems(new EcsWorld(), new shared.Shared());

        var baseEcsSystemsBuilder = new BaseEcsSystemsBuilder();
        
        var timersFeatureBuilder = new TimersSystemsBuilder();
        var cardsFeatureBuilder = new CardsSystemsBuilder();

        baseEcsSystemsBuilder.BuildDataProvidersSystems(Systems);
        timersFeatureBuilder.BuildDataProvidersSystems(Systems);
        cardsFeatureBuilder.BuildDataProvidersSystems(Systems);
            
        baseEcsSystemsBuilder.BuildBaseSystems(Systems);
        timersFeatureBuilder.BuildBaseSystems(Systems);
        cardsFeatureBuilder.BuildBaseSystems(Systems);
        
        baseEcsSystemsBuilder.BuildMiddleSystems(Systems);
        timersFeatureBuilder.BuildMiddleSystems(Systems);
        cardsFeatureBuilder.BuildMiddleSystems(Systems);

        timersFeatureBuilder.BuildPostSystems(Systems);
        cardsFeatureBuilder.BuildPostSystems(Systems);
        baseEcsSystemsBuilder.BuildPostSystems(Systems);

#if UNITY_EDITOR
        var debugFeatureBuilder = new LeoEcsDebugFeatureBuilder();
        debugFeatureBuilder.BuildBaseSystems(Systems);
        debugFeatureBuilder.BuildMiddleSystems(Systems);
        debugFeatureBuilder.BuildPostSystems(Systems);
#endif
            
        Systems.InjectShared(contentManager);

        Systems.InitShared();
        Systems.Init();
    }
        
    private async UniTask InitContentManager()
    {
        contentManager = new ContentManager();
        await contentManager.Init();
        await contentManager.PreloadAssets();
    }

    private void Update()
    {
        Systems?.Run();
    }
}
